#!/usr/bin/env bash
## vars
mariadb_release='10.6'
memcached_release='1.6'
seafile_image='git.garbaye.fr/garbaye/seafile-mc'
seafile_release='11.0.13'
## default vars : override with ENV var
listen_if="${GARBAYE_SEAFILE_ENV_LISTENIF:-127.0.0.1}"
listen_port="${GARBAYE_SEAFILE_ENV_LISTENPORT:-8082}"
let "listen_ws_port = ${listen_port} + 1"
GARBAYE_SEAFILE_ADMIN_PASSWORD="${GARBAYE_SEAFILE_ENV_ADMIN_PASSWORD:-asecret}"
## mandatory ENV vars
envvars='GARBAYE_SEAFILE_ADMIN_EMAIL GARBAYE_SEAFILE_SERVER_HOSTNAME GARBAYE_SEAFILE_MYSQL_ROOT_PASSWORD'
## internal vars : do not touch
project_name=${PWD##*/}
pod_name="pod_${project_name}"
service_name="pod-${pod_name}.service"
get_default_iface_ipv4 GARBAYE_SEAFILE_SMTP_SERVER
upstream_images="${seafile_image} docker.io/library/mariadb docker.io/library/memcached"
dbvolume='podman-seafile_seafile-db'
datavolume='podman-seafile_seafile-data'
