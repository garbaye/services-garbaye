---
name: "Nouveau service"
about: "Vous souhaitez que Garbaye propose un nouveau service? C'est ici."
title: "[Nouveau Service] "
labels:
- type:projet
---
## Besoin et bénéfices
<!-- Décrire le besoin rencontré, le logiciel choisi et ses alternatives connues. -->

## Opportunité
<!-- Décrire en quoi il est opportun de choisir ce logiciel (histoire, activité, popularité, langage utilisé, tendance actuelle) -->

## Contraintes et risques
<!-- Décrire les contraintes et risques à anticipes : capacitif (CPU/RAM/stockage) et légal (utilisation détournée par des utilisateurs malveillants) -->

## Accès au service
<!-- Décrire le niveau d'accès souhaité (tout public, compte libre, compte sur demande, etc.) . -->

<!-- Merci, vous pouvez créer l'issue. La suite n'est pas à compléter. -->


Avec ces éléments, la Gouvernance Garbaye (GG) choisit d'explorer ou pas le service.
- [ ] GG : OK pour EXPLORATION (assigner un tag de priorité)
- [ ] GG : Arrêt du projet (justifier en commentaire)

## Gestation nouveau service

### Phase EXPLORATION
- [ ] Réservation du nom DNS (choix non définitif) `dev-NOMSERVICE`
- [ ] Avec le compte `podman-user` sur un noeud `compute`, réaliser les développements dans le dépot `service-garbaye` et déployer le pod/container. Ne pas créer d'utilisateur, home, partition ou réservation de port à cette étape.

Selon l'évaluation du service, la GG choisit de poursuivre avec la mise en QUALIFICATION du service :
- [ ] GG : OK pour QUALIFICATION
- [ ] GG : Arrêt du projet (justifier en commentaire)

### Phase QUALIFICATION :
- [ ] GG : Choix des noms définitifs du service (DNS) et interne (utilisateur et application)
- [ ] Réservation des UID/GID d'utilisateur `ansible-garbaye/garbaye_stuff/uid-gid.md`
   - [ ] Création des utilisateurs `ansible-garbaye/group_vars/compute`
- [ ] Réservation des ports réseau `ansible-garbaye/garbaye_stuff/ports.md`
- [ ] Réservation du nom DNS (qlf-NOMSERVICE.garbaye.fr)
   - [ ] Ajuster les `ansible-garbaye/host_vars/SERVEUR_DE_QUALIFICATION` pour faire fonctionner Nginx et Certbot, déployer avec le playbook `webfront.yml`.
- [ ] Renseigner les variables pour l'environnement de QUALIFICATION dans `ansible-garbaye/garbaye_stuff/envvars.txt`
- Sur le serveur qui portera l'environnement de QUALIFICATION :
   - [ ] (si nécessaire) Création du LV pour le home, migration du `home` utilisateur (attention aux permissions et SELinux), modification `/etc/fstab`.
   - [ ] Avec le compte utilisateur lié, déployer le service.

### Phase TUNING
Sur l'environnement de QUALIFICATION, on observe une phase d'utilisation afin :
- [ ] De tester les scripts de gestion du service (ne pas oublier les montées de version).
- [ ] De rendre paramétrable la configuration du service (fichier de configuration, variables d'environnement, etc.).
- [ ] La GG choisit les paramétrages prévus pour la mise en production.

La GG statue sur la mise en PRODUCTION :
- [ ] GG : OK pour PRODUCTION
- [ ] GG : Arrêt du projet (justifier en commentaire)

### Phase PRODUCTION
- [ ] Réservation du nom DNS (NOMSERVICE.garbaye.fr)
   - [ ] Ajuster les `ansible-garbaye/host_vars/SERVEUR_DE_PRODUCTION` pour faire fonctionner Nginx et Certbot, déployer avec le playbook `webfront.yml`.
- [ ] Renseigner les variables pour l'environnement de PRODUCTION dans `ansible-garbaye/garbaye_stuff/envvars.txt`

Sur le serveur qui portera l'environnement de PRODUCTION :
- [ ] Création du LV pour le home, migration du `home` utilisateur (attention aux permissions et SELinux), modification `/etc/fstab`.
- [ ] Avec le compte utilisateur lié, déployer le service.

Étapes supplémentaires par rapport à la QUALIFICATION :
- [ ] Mise en place des backups rsnasphot (databases) et vérification.
- [ ] [Status](https://status.garbaye.fr) : ajout du service IPv4 et IPv6 à `tinystatus`
- [ ] Supervision : ajout du service IPv4 et IPv6 à `icinga2`

### Phase FINALISATION
- [ ] Ajouter le service à la page d'accueil du [site web](https://garbaye.fr)
- [ ] Ajouter le service à chatonsinfos : `www-garbaye/static/.well-known/chatonsinfos/service-NOMSERVICE.properties`
- [ ] Créer une Fiche Service Chatons (demander de l'aide)
- [ ] Annonce publique : créer une entrée de blog sur le [site web](https://garbaye.fr)