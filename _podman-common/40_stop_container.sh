#!/usr/bin/env bash

ABSDIR="$( dirname "$(realpath -s -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_container_exists "${container_name}"

# Check of running through systemd (as it should be)
if check_systemd_unit_exists "${service_name}"; then
    if check_systemd_unit_running "${service_name}"; then
	systemctl --user stop "${service_name}" &&
	echo Systemd service "${service_name}" stopped.
    fi
    # Leave if container is correctly stopped
    if ! check_container_running "${container_name}"; then
	exit 0
    fi
fi

# Check if running through podman (no systemd), stop with podman container stop , then exit
if check_container_running "${container_name}"; then
  echo Container found running without systemd unit, stopping it now.
  podman container stop "${container_name}"
  exit 0
else
  echo Container "${container_name}" is not running.
  exit 1
fi
