#!/usr/bin/env bash

ABSDIR="$( dirname "$(realpath -s -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

# FAIL if pod does not exists
ensure_pod_exists ${pod_name}

# FAIL if pod is running
ensure_pod_not_running ${pod_name}

${ABSDIR}/70_disable.sh

rm -f ${HOME}/.config/systemd/user/${service_name}
systemctl --user daemon-reload
podman pod rm ${pod_name}

for volume in ${nonpersistent_volumes}; do
    podman volume rm ${volume}
done
