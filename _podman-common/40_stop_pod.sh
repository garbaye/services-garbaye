#!/usr/bin/env bash

ABSDIR="$( dirname "$(realpath -s -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_pod_exists ${pod_name}

# Check of running through systemd (as it should be)
if check_systemd_unit_exists ${service_name}; then
    if check_systemd_unit_running ${service_name}; then
	systemctl --user stop ${service_name} &&
	echo Systemd service ${service_name} stopped.
    fi
    # Leave if pod is correctly stopped
    if ! check_pod_running ${pod_name}; then
	exit 0
    fi
fi

# Check if running through podman (no systemd), stop with podman pod stop , then exit
if check_pod_running ${pod_name}; then
  echo Pod found running without systemd unit, stopping it now.
  podman pod stop ${pod_name}
  exit 0
else
  echo Pod ${pod_name} is not running.
  exit 1
fi
