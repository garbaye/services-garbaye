#!/usr/bin/env bash

ABSDIR="$( dirname "$(realpath -s -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

# FAIL if container does not exists
ensure_container_exists "${container_name}"

# FAIL if systemd unit does not exists
ensure_systemd_unit_exists "${service_name}"

# FAIL if systemd unit is running (stop it first)
ensure_systemd_unit_not_running "${service_name}"

systemctl --user disable "${service_name}"
