#!/usr/bin/env bash
## vars
ntfy_image="git.garbaye.fr/garbaye/ntfy"
ntfy_version='v2.10.0'
## default vars : override with ENV var
listen_if="${GARBAYE_NTFY_ENV_LISTENIF:-127.0.0.1}"
listen_port="${GARBAYE_NTFY_ENV_LISTENPORT:-8091}"
## mandatory ENV vars
envvars='GARBAYE_NTFY_BASE_URL'
## internal vars : do not touch
project_name=${PWD##*/}
container_name="${project_name}"
service_name="container-${container_name}.service"
ntfy_data_volume='ntfy-data'
