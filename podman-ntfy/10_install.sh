#!/usr/bin/env bash

ABSDIR="$( dirname "$(realpath -s -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_container_not_exists ${container_name}
ensure_variables_are_defined "$envvars"

if ! podman volume exists ${ntfy_data_volume} ; then
    echo "Error : data volume ${ntfy_data_volume} does not exists. Consider running 05_freshinstall.sh if this is the first install."
    exit 1
fi

if ! podman image exists ${ntfy_image}:${ntfy_version}; then
    podman image pull ${ntfy_image}:${ntfy_version} || exit 1
fi

podman container run --name ${container_name} \
       -p ${listen_if}:${listen_port}:8080 \
       -e TZ='Europe/Paris' \
       -v ${ntfy_data_volume}:/var/lib/ntfy:Z \
       -d ${ntfy_image}:${ntfy_version} serve \
       --listen-http :8080 --base-url ${GARBAYE_NTFY_BASE_URL} &&
( podman container logs -f ${container_name} 2>&1 & ) | grep -q 'INFO Listening on' &&
podman container cp config/server.yml ${container_name}:/etc/ntfy/server.yml &&
podman container stop ${container_name} &&
echo Container ${container_name} successfully built and stopped.
