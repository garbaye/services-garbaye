#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_variables_are_defined "$envvars"

if podman volume exists ${ntfy_data_volume}; then
	echo "Files or volumes from previous installation already exists"
	echo "Please remove them before fresh install, or try continue with normal installation"
	exit 1
fi

podman volume create ${ntfy_data_volume}
podman unshare chown -R 100:65533 `get_podman_volume_path ${ntfy_data_volume}`
${ABSDIR}/10_install.sh
