#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_variables_are_defined "$envvars"

if podman volume exists ${confvolume} || podman volume exists ${dbvolume}; then
	echo "Files or volumes from previous installation already exists"
	echo "Please remove them before fresh install, or try continue with normal installation"
	exit 1
fi

if ! podman image exists ${synapse_image}:${synapse_version}; then
    podman image pull ${synapse_image}:${synapse_version} || exit 1
fi
podman volume create ${confvolume} &&
podman volume create ${dbvolume} &&
podman container run -it --rm --user 991:991 --name ${container_name}-generate --mount type=volume,src=${confvolume},dst=/data -e SYNAPSE_SERVER_NAME=${GARBAYE_MATRIX_DOMAIN} -e SYNAPSE_REPORT_STATS=no ${synapse_image}:${synapse_version} generate &&
podman_unshare_sed_in_place "^pid_file: /data/homeserver.pid$" "pid_file: /data/homeserver.pid\npublic_baseurl: ${GARBAYE_MATRIX_URL}\nallow_public_rooms_over_federation: true" `get_podman_volume_path ${confvolume}`/homeserver.yaml &&
podman_unshare_sed_in_place_multiline "database:\n  name: sqlite3\n  args:\n    database: /data/homeserver.db" "database:\n  name: psycopg2\n  args:\n    user: synapse\n    password: ${GARBAYE_MATRIX_POSTGRES_PASSWORD}\n    database: synapse\n    host: synapse-db\n    port: 5432\n    cp_min: 5\n    cp_max: 10\n    cp_reconnect: true" `get_podman_volume_path ${confvolume}`/homeserver.yaml &&
${ABSDIR}/10_install.sh
