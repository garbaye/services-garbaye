#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_pod_not_exists ${pod_name}
ensure_variables_are_defined "$envvars"

if ! podman volume exists ${confvolume} ; then
    echo "Error : conf volume ${confvolume} does not exists. Consider running 05_freshinstall.sh if this is the first install."
    exit 1
fi

if ! podman volume exists ${dbvolume} ; then
    echo "Error : conf volume ${dbvolume} does not exists. Consider running 05_freshinstall.sh if this is the first install."
    exit 1
fi

cat <<EOT >> .env
POSTGRES_PASSWORD=${GARBAYE_MATRIX_POSTGRES_PASSWORD}
POSTGRES_USER=synapse
POSTGRES_DB=synapse
POSTGRES_INITDB_ARGS=--encoding=UTF8 --locale=C
EOT

export postgres_image
export postgres_version
export synapse_image
export synapse_version
export listen_if
export listen_port

if ! podman image exists ${synapse_image}:${synapse_version}; then
    podman image pull ${synapse_image}:${synapse_version} || exit 1
fi
podman image pull ${postgres_image}:${postgres_version} &&
${my_podman_compose} --pod-args="--infra=true --infra-name=${project_name}_infra --share=" --podman-run-args "--requires=${project_name}_infra --env-file .env" up -d &&

end=$((SECONDS+120))
while [ $SECONDS -lt $end ]; do
    sleep 1
    if check_container_running ${container_name}; then
	podman logs ${container_name} 2>&1 | grep -qe 'Preparing for databases' &&
	break
    else
	podman logs ${container_name} 2>&1 | grep -qe 'psycopg2.OperationalError: could not connect to server: Connection refused' &&
        podman healthcheck run ${db_container_name} &&
	echo "Oops, synapse didn't wait for PostgreSQL, restarting"
        podman start ${container_name}
    fi
done &&

podman container wait --condition=running ${container_name} &&
echo -n "Waiting for background tasks to end... " &&
( podman container logs --tail=50 -f ${container_name} 2>&1 & ) | grep -q 'No more background updates to do' &&
echo "OK." || retval=false
podman pod stop ${pod_name} &&
echo "Done. Pod stopped."
shred -u .env
eval "$retval"
