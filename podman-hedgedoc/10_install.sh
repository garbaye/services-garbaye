#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_pod_not_exists ${pod_name}
ensure_variables_are_defined "$envvars"

if ! podman volume exists ${dbvolume} ; then
    echo "Error : DB volume ${dbvolume} does not exists. Consider running 05_freshinstall.sh if this is the first install."
    exit 1
fi

if ! podman volume exists ${datavolume} ; then
    echo "Error : DATA volume ${datavolume} does not exists. Consider running 05_freshinstall.sh if this is the first install."
    exit 1
fi

cat <<EOT > .env
# HedgeDoc https://docs.hedgedoc.org/configuration/
## Node.JS
NODE_ENV=production
DEBUG=false
## HedgeDoc basics
#CMD_CONFIG_FILE=/data/config.json
CMD_DB_USERNAME=hedgedoc
CMD_DB_PASSWORD=${GARBAYE_HEDGEDOC_DATABASE_PASSWORD}
CMD_DB_DATABASE=hedgedoc
CMD_DB_HOST=database
CMD_DB_PORT=${database_port}
CMD_DB_DIALECT=${database_dialect}
#CMD_DB_URL=sqlite:///data/sqlite.db
#CMD_DB_URL=CMD_DB_URL=postgres://hedgedoc:${GARBAYE_HEDGEDOC_DATABASE_PASSWORD}@database:5432/hedgedoc
#CMD_DB_URL=CMD_DB_URL=mysql://hedgedoc:${GARBAYE_HEDGEDOC_DATABASE_PASSWORD}@database:3306/hedgedoc
CMD_IMAGE_UPLOAD_TYPE=filesystem
## HedgeDoc Location
CMD_DOMAIN=${GARBAYE_HEDGEDOC_DOMAIN}
CMD_PROTOCOL_USESSL=true
CMD_URL_ADDPORT=false
CMD_ALLOW_ORIGIN=["${GARBAYE_HEDGEDOC_DOMAIN}"]
## Web security aspects
CMD_CSP_ALLOW_FRAMING=false
CMD_CSP_ALLOW_PDF_EMBED=false
CMD_COOKIE_POLICY=strict
## Privacy and External Requests
CMD_ALLOW_GRAVATAR=false
## Users and Privileges
CMD_ALLOW_ANONYMOUS=true
CMD_ALLOW_ANONYMOUS_EDITS=true
## Login methods
CMD_ALLOW_EMAIL_REGISTER=false
# PostgreSQL
POSTGRES_DB=hedgedoc
POSTGRES_PASSWORD=${GARBAYE_HEDGEDOC_DATABASE_PASSWORD}
POSTGRES_USER=hedgedoc
# MySQL
MYSQL_USER=hedgedoc
MYSQL_PASSWORD=${GARBAYE_HEDGEDOC_DATABASE_PASSWORD}
MYSQL_DATABASE=hedgedoc
MYSQL_RANDOM_ROOT_PASSWORD=true
EOT

export hedgedoc_image
export hedgedoc_version
export database_image
export database_version
export database_path
export container_name
export db_container_name

if ! podman image exists ${hedgedoc_image}:${hedgedoc_version}; then
    podman image pull ${hedgedoc_image}:${hedgedoc_version} || exit 1
fi
if ! podman image exists ${database_image}:${database_version}; then
    podman image pull ${database_image}:${database_version} || exit 1
fi
${my_podman_compose} --pod-args="--infra=true --infra-name=${project_name}_infra --share=" --podman-run-args "--requires=${project_name}_infra --env-file .env" up -d &&
echo -n "Waiting for hedgedoc to finish starting " &&
( podman container logs -f ${container_name} 2>&1 & ) | grep -q 'HTTP Server listening at ' &&
echo "OK" &&
podman pod stop ${pod_name} &&
echo Pod built and stopped. &&
shred -u .env
