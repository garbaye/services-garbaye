#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

buildfolder=/tmp/hedgedoc-$$

if ! podman image exists ${hedgedoc_image}:${hedgedoc_version}; then
    mkdir ${buildfolder} &&
    if git clone --depth=1 https://github.com/hedgedoc/container ${buildfolder}/ ; then
        rm -rf "${HOME}/buildah-cache-${UID}"
        TMPDIR=${HOME} podman image build \
	      -t ${hedgedoc_image}:${hedgedoc_version} \
	      -f ${buildfolder}/alpine/Dockerfile \
	      ${buildfolder}/ || retval=false
        podman image prune -a -f --filter dangling=true
        podman image prune -a -f --filter intermediate=true
        podman image rm -f $(podman image list -a -q -- docker.io/library/node)
    fi
    rm -rf ${buildfolder}
    eval "$retval"
else
    echo "Image ${hedgedoc_image}:${hedgedoc_version} already built"
fi &&

oci_push_to_registry ${hedgedoc_image}:${hedgedoc_version}
