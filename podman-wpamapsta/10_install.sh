#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_pod_not_exists ${pod_name}
ensure_variables_are_defined "$envvars"

for volume in ${dbvolume} ${wpvolume}; do
    if ! podman volume exists ${volume} ; then
	echo "Error : conf volume ${volume} does not exists. Consider running 05_freshinstall.sh if this is the first install."
	exit 1
    fi
done

cat <<EOT >> .env
MARIADB_PASSWORD=${GARBAYE_WPAMAPSTA_MYSQL_PASSWORD}
WORDPRESS_DB_PASSWORD=${GARBAYE_WPAMAPSTA_MYSQL_PASSWORD}

EOT

export wp_image
export wp_version
export mysql_image
export mysql_version
export listen_if
export listen_port

if ! podman image exists ${wp_image}:${wp_version}; then
    podman image pull ${wp_image}:${wp_version} || exit 1
fi
podman image pull ${mysql_image}:${mysql_version} &&

${my_podman_compose} --pod-args="--infra=true --infra-name=${project_name}_infra --share=" --podman-run-args "--requires=${project_name}_infra --env-file .env" up -d &&
sleep 10 # TODO : wait for healthckeck on mariadb
shred -u .env &&
podman pod stop ${pod_name}
