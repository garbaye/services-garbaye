#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

php='php8.2'
flavor='apache'
buildfolder=/tmp/wp-$$

mkdir ${buildfolder} &&
if git clone --depth=1 https://github.com/docker-library/wordpress.git ${buildfolder} ; then
    wp_version=$(grep -Po "version=\'\K[0-9.]*" ${buildfolder}/latest/${php}/${flavor}/Dockerfile)
    if ! podman image exists ${wp_image}:${wp_version}-${php}-${flavor}; then
	TMPDIR=${HOME} podman build \
	      -t ${wp_image}:${wp_version}-${php}-${flavor} \
	      -f ${buildfolder}/latest/${php}/${flavor}/Dockerfile \
	      ${buildfolder}/latest/${php}/${flavor}/ || retval=false
	podman image prune -a -f --filter dangling=true
	podman image prune -a -f --filter intermediate=true
    fi
    rm -rf ${buildfolder}
    eval "$retval"
else
    echo "Image ${wp_image}:${wp_version}-${php}-${flavor} already built"
fi &&

oci_push_to_registry ${wp_image}:${wp_version}-${php}-${flavor}
