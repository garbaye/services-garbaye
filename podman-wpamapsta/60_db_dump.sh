#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_not_root
ensure_scriptdir_equals_user

# pod should exist
ensure_pod_exists "${pod_name}"

podman exec db /bin/bash -c '/usr/bin/mysqldump --user=${MARIADB_USER} --password=${MARIADB_PASSWORD} --all-databases'
