#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_pod_not_exists ${pod_name}
ensure_variables_are_defined "$envvars"

if ! podman volume exists ${datavolume} ; then
    echo "Error : DATA volume ${datavolume} does not exists. Consider running 05_freshinstall.sh if this is the first install."
    exit 1
fi

cat <<EOT > .env
# vaultwarden
ADMIN_TOKEN=${GARBAYE_VAULTWARDEN_ADMIN_PASSWORD}
DOMAIN=${GARBAYE_VAULTWARDEN_DOMAIN}
SENDS_ALLOWED=false
IP_HEADER=X-Forwarded-For
# Signups
SIGNUPS_ALLOWED=true
SIGNUPS_VERIFY=true
# SMTP
SMTP_HOST=${GARBAYE_SMTP_SERVER}
SMTP_FROM=vaultwarden@garbaye.fr
SMTP_FROM_NAME=Vaultwarden
SMTP_SECURITY=off
SMTP_PORT=25
EOT

export service_image
export service_version
export container_name
export listen_if
export listen_port

if ! podman image exists ${service_image}:${service_version}; then
    podman image pull ${service_image}:${service_version} || exit 1
fi
${my_podman_compose} --pod-args="--infra=true --infra-name=${project_name}_infra --share=" --podman-run-args "--requires=${project_name}_infra --env-file .env" up -d &&
echo -n "Waiting for vaultwarden to finish starting " &&
( podman container logs -f ${container_name} 2>&1 & ) | grep -q 'Rocket has launched from ' &&
echo "OK" &&
podman pod stop ${pod_name} &&
echo Pod built and stopped. &&
shred -u .env
