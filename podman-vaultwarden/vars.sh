#!/usr/bin/env bash
## vars
service_image="git.garbaye.fr/garbaye/vaultwarden"
service_version='1.33.2-amd64'
## default vars : override with ENV var
GARBAYE_VAULTWARDEN_DOMAIN="${GARBAYE_VAULTWARDEN_ENV_DOMAIN:-http://localhost}"
listen_if="${GARBAYE_VAULTWARDEN_ENV_LISTENIF:-127.0.0.1}"
listen_port="${GARBAYE_VAULTWARDEN_ENV_LISTENPORT:-8090}"
## mandatory ENV vars
envvars='GARBAYE_VAULTWARDEN_ADMIN_PASSWORD'
## internal vars : do not touch
project_name=${PWD##*/}
pod_name="pod_${project_name}"
service_name="pod-${pod_name}.service"
upstream_images="${service_image}"
datavolume="${project_name}_data"
container_name="${project_name}_app"
get_default_iface_ipv4 GARBAYE_SMTP_SERVER
