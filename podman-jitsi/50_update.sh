#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_pod_exists ${pod_name}
ensure_systemd_unit_exists ${service_name}
ensure_variables_are_defined "$envvars"

current_version=$(podman container list -a --format "{{.Image}}" | grep prosody | awk -F: '{print $NF}')

reinstall_please () {
    for image in ${upstream_images}; do
	if ! podman image exists ${image}:${version}; then
	    podman image pull ${image}:${version} || exit 1
	fi
    done &&
    check_pod_running ${pod_name} && ./40_stop.sh
    ./70_disable.sh && \
    ./80_destroy.sh && \
    ./10_install.sh && \
    ./20_enable.sh && \
    ./30_start.sh
}

cleanup_images () {
    echo "Remove ${current_version} images?"
    select yn in "Yes" "No"; do
	case $yn in
	    Yes)
		for image in ${upstream_images}; do
		    podman image rm ${image}:${current_version}
		done
		exit 0
		;;
	    No)
		exit 0
		;;
	esac
    done
}

if [ ${current_version} != ${version} ]; then
    if [[ "${current_version}" > "${version}" ]]; then
	echo "WARNING : you are about to DOWNGRADE your installation"
    fi
    echo "Migrating from ${current_version} to ${version}. Proceed?"
    select yn in "Yes" "No"; do
	case $yn in
	    Yes)
		reinstall_please && \
		cleanup_images
		exit 0
		;;
	    No)
		exit 0
		;;
	esac
	done
else
    echo "Already using version ${version}. Exiting."
fi
