#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source "${ABSDIR}"/../functions.sh
source "${ABSDIR}"/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

buildfolder=/tmp/docker-jitsi-meet-$$

build_base() {
    podman image pull docker.io/library/debian:bullseye-slim
    if ! podman image exists localhost/jitsi/base:${version}; then
        TMPDIR=${HOME} podman image build -t localhost/jitsi/base:${version} ${buildfolder}/base/ &&
        podman image tag localhost/jitsi/base:${version} localhost/jitsi/base:latest || exit 255
    fi
    if ! podman image exists localhost/jitsi/base-java:${version}; then
        TMPDIR=${HOME} podman image build -t localhost/jitsi/base-java:${version} ${buildfolder}/base-java &&
        podman image tag localhost/jitsi/base-java:${version} localhost/jitsi/base-java:latest || exit 255
    fi
    base_cleanup=true
}

mkdir ${buildfolder} &&
if git clone -b ${version} --depth=1 https://github.com/jitsi/docker-jitsi-meet/ ${buildfolder} ; then
    if ! podman image exists git.garbaye.fr/garbaye/jitsi-jvb:${version}; then
	build_base
        TMPDIR=${HOME} podman image build -t git.garbaye.fr/garbaye/jitsi-jvb:${version} ${buildfolder}/jvb/ || exit 255
    fi
    if ! podman image exists git.garbaye.fr/garbaye/jitsi-jicofo:${version}; then
	build_base
        TMPDIR=${HOME} podman image build -t git.garbaye.fr/garbaye/jitsi-jicofo:${version} ${buildfolder}/jicofo/ || exit 255
    fi
    if ! podman image exists git.garbaye.fr/garbaye/jitsi-prosody:${version}; then
	build_base
        TMPDIR=${HOME} podman image build -t git.garbaye.fr/garbaye/jitsi-prosody:${version} ${buildfolder}/prosody/ || exit 255
    fi
    if ! podman image exists git.garbaye.fr/garbaye/jitsi-web:${version}; then
	build_base
        TMPDIR=${HOME} podman image build -t git.garbaye.fr/garbaye/jitsi-web:${version} ${buildfolder}/web/ || exit 255
    fi
    podman image prune -a -f --filter dangling=true
    podman image prune -a -f --filter intermediate=true
    if [ -n "${base_cleanup}" ] ; then
	podman image rm localhost/jitsi/base-java:${version} localhost/jitsi/base-java:latest
	podman image rm localhost/jitsi/base:${version} localhost/jitsi/base:latest
    fi
    podman image rm -f $(podman image list -a -q -- docker.io/library/debian)
fi
rm -rf ${buildfolder}
for image in ${upstream_images}; do
    oci_push_to_registry "${image}":${version}
done
