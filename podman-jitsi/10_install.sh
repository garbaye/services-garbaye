#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

ensure_pod_not_exists ${pod_name}
ensure_variables_are_defined "$envvars"

if [[ -d ${confvolume} ]]; then
    echo "Error : conf volume ${confvolume} already exists. Please consider running 80_destroy.sh."
    exit 1
fi

for image in ${upstream_images}; do
    if ! podman image exists ${image}:${version}; then
	podman image pull ${image}:${version} || exit 1
    fi
done &&

mkdir -p ${confvolume}/{web/letsencrypt,web/crontabs,transcripts,prosody/config,prosody/prosody-plugins-custom,jicofo,jvb,jigasi,jibri}

# Patch config.js
echo "
config.enableInsecureRoomNameWarning = false;

config.disableThirdPartyRequests = true;
" > ${confvolume}/web/custom-config.js

# Patch ${confvolume}/web/interface_config.js
echo "
interfaceConfig.APP_NAME = 'Jitsi Garbaye';
interfaceConfig.DEFAULT_REMOTE_DISPLAY_NAME = 'Participant';
interfaceConfig.JITSI_WATERMARK_LINK = '${GARBAYE_JITSI_URL}';
" > ${confvolume}/web/custom-interface_config.js

curl -s -- "https://codeload.github.com/jitsi/docker-jitsi-meet/tar.gz/refs/tags/${version}" | tar xzv --strip-components 1 docker-jitsi-meet-${version}/docker-compose.yml docker-jitsi-meet-${version}/env.example docker-jitsi-meet-${version}/gen-passwords.sh
cp env.example .env
./gen-passwords.sh

# Patch docker-compose.yml
sed_in_place "image: jitsi/" "image: git.garbaye.fr/garbaye/jitsi-" docker-compose.yml
sed_in_place "^            - '\${HTTP_PORT}:80'" "            - '\${listen_if}:\${HTTP_PORT}:80'" docker-compose.yml
sed_in_place "^            - '\${HTTPS_PORT}:443'" "#            - '\${HTTPS_PORT}:443'" docker-compose.yml

# Patch env file
sed_in_place "^HTTP_PORT=8000" "HTTP_PORT=${listen_port}" .env
sed_in_place "^HTTPS_PORT=8443" "HTTPS_PORT=443" .env
sed_in_place "^TZ=UTC" "TZ=Europe/Paris" .env
sed_in_place "^#PUBLIC_URL=https://meet.example.com:\${HTTPS_PORT}" "PUBLIC_URL=${GARBAYE_JITSI_URL}" .env
sed_in_place "^#JVB_ADVERTISE_IPS=192.168.1.1,1.2.3.4" "JVB_ADVERTISE_IPS=${GARBAYE_JITSI_PRIV_IP}" .env
sed_in_place "^#ENABLE_LETSENCRYPT=1" "ENABLE_LETSENCRYPT=0" .env

echo "ENABLE_P2P=false" >> .env
echo "XMPP_SERVER=prosody" >> .env
echo "XMPP_BOSH_URL_BASE=http://prosody:5280" >> .env
echo "JVB_PORT=${listen_port}" >> .env
echo "JICOFO_REST_PORT=${jicofo_port}" >> .env
echo "JVB_COLIBRI_PORT=${colibri_port}" >> .env
echo "DISABLE_HTTPS=1" >> .env
echo "ENABLE_IPV6=0" >> .env
echo "COLIBRI_REST_ENABLED=1" >> .env

export listen_if
export listen_port

${my_podman_compose} --pod-args "--infra=true --infra-name=${project_name}_infra --share=" --podman-run-args "--requires=${project_name}_infra --env-file .env" up -d &&

# Wait for web to be up
( podman container logs --tail=3 -f ${project_name}_web_1 2>&1 & ) | grep -q '^\[services.d\] done.$' &&
podman container cp assets/welcome-background.png ${project_name}_web_1:/usr/share/jitsi-meet/images/welcome-background.png &&
podman container exec -t ${project_name}_web_1 chmod 444 /usr/share/jitsi-meet/images/welcome-background.png &&
podman pod stop ${pod_name} && echo Pod built and stopped.

# cleanup
shred -u .env .env.bak env.example gen-passwords.sh docker-compose.yml
