# Services Garbaye
[![pipeline status](https://framagit.org/garbaye/services-garbaye/badges/main/pipeline.svg)](https://framagit.org/garbaye/services-garbaye/-/commits/main) 
## Pré-requis
- Système GNU/Linux
- [Podman](https://podman.io/) 4.0+ en utilisateur
  - backend réseau netavark + aardvark-dns
- systemd avec [Lingering](https://www.freedesktop.org/software/systemd/man/loginctl.html) activé pour l'utilisateur
- [podman-compose](https://github.com/containers/podman-compose) commit 7f5ce26b1b8103f48d5702dce7d52afe8b76daa5
- SELinux supporté (optionnel)
