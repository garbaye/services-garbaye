#!/usr/bin/env bash
## vars
framadate_image='git.garbaye.fr/garbaye/framadate-app'
framadate_version='1.1.19'
mysql_image='docker.io/library/mariadb'
mysql_version='10.10'
## default vars : override with ENV var
listen_if="${GARBAYE_FRAMADATE_ENV_LISTENIF:-127.0.0.1}"
listen_port="${GARBAYE_FRAMADATE_ENV_LISTENPORT:-8087}"
GARBAYE_FRAMADATE_APP_NAME="${GARBAYE_FRAMADATE_ENV_APP_NAME:-Framadate}"
## mandatory ENV vars
envvars='GARBAYE_FRAMADATE_DOMAIN GARBAYE_FRAMADATE_ADMIN_MAIL GARBAYE_FRAMADATE_ADMIN_PASSWORD GARBAYE_FRAMADATE_MYSQL_ROOT_PASSWORD GARBAYE_FRAMADATE_MYSQL_PASSWORD'
## internal vars : do not touch
project_name=${PWD##*/}
pod_name="pod_${project_name}"
service_name="pod-${pod_name}.service"
get_default_iface_ipv4 GARBAYE_FRAMADATE_SMTP_SERVER
upstream_images="${framadate_image} docker.io/library/composer docker.io/library/mariadb docker.io/library/php docker.io/library/composer"
dbvolume='podman-framadate_framadate-db'

