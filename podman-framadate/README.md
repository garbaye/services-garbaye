# Framadate
## Restauration
À partir d'une copie des home utilisateur (volumes podman compris) dans `/backup` :
* En tant qu'utilisateur `podman-framadate` :
```
podman volume create podman-framadate_framadate-db
```
* En tant que `root` :
```
rsync -a /backup/home/podman-framadate/.local/share/containers/storage/volumes/podman-framadate_framadate-db/_data ~podman-framadate/.local/share/containers/storage/volumes/podman-framadate_framadate-db/
chown -R podman-framadate:podman-users ~podman-framadate/.local/share/containers/storage/volumes/podman-framadate_framadate-db/_data/
```
* En tant qu'utilisateur `podman-framadate` :
```
podman unshare chown -R 999:999 ~/.local/share/containers/storage/volumes/podman-framadate_framadate-db/_data
```
* Exporter les variables d'environnement et procéder à l'installation normale (10_install.sh)
