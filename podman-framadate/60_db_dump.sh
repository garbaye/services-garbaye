#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_not_root
ensure_scriptdir_equals_user

# pod should exist
ensure_pod_exists "${pod_name}"

podman exec framadate-db /bin/bash -c '/usr/bin/mysqldump -uroot --password=${MYSQL_ROOT_PASSWORD} --all-databases'
