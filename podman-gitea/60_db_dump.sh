#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_not_root
ensure_scriptdir_equals_user

# pod should exist
ensure_pod_exists "${pod_name}"

db=${1}

podman exec gitea-db /bin/bash -c "PGPASSWORD=\${POSTGRES_PASSWORD} pg_dump -w -F t -U \${POSTGRES_USER} ${db}"
