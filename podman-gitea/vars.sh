#!/usr/bin/env bash
## vars
gitea_image="git.garbaye.fr/garbaye/forgejo"
gitea_version='7.0.13'
postgres_image="docker.io/library/postgres"
postgres_version='14'
## default vars : override with ENV var
weblisten_if="${GARBAYE_GITEA_ENV_WEBLISTENIF:-127.0.0.1}"
weblisten_port="${GARBAYE_GITEA_ENV_WEBLISTENPORT:-8088}"
sshlisten_if="${GARBAYE_GITEA_ENV_SSHLISTENIF:-0.0.0.0}"
sshlisten_port="${GARBAYE_GITEA_ENV_SSHLISTENPORT:-2222}"
GARBAYE_GITEA_APPNAME="${GARBAYE_GITEA_ENV_APPNAME:-Gitea}"
## mandatory ENV vars
envvars='GARBAYE_GITEA_DOMAIN GARBAYE_GITEA_POSTGRES_PASSWORD GARBAYE_GITEA_SMTP_FROM'
## internal vars : do not touch
project_name=${PWD##*/}
pod_name="${project_name}"
pod_name="pod_${project_name}"
service_name="pod-${pod_name}.service"
get_default_iface_ipv4 GARBAYE_GITEA_SMTP_ADDR
upstream_images="${gitea_image} ${postgres_image}"
confvolume='podman-gitea_gitea-config'
datavolume='podman-gitea_gitea-data'
dbvolume='podman-gitea_gitea-pgsql'
container_name='gitea'
db_container_name='gitea-db'
