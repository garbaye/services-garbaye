#!/usr/bin/env bats

@test "[http] Check website is responding" {
  run -0 curl -s4I "http://localhost:${weblisten_port}/user/login?redirect_to=/"
  echo "${output}" | grep -q 'HTTP/1.1 200 OK'
}

@test "[http] Check website is well configured" {
  run -0 curl -s "http://localhost:${weblisten_port}/"
  echo "${output}" | grep -q '<title>La forge Garbaye</title>'
}

@test "[gitea] Check gitea doctor for errors" {
  skip
  run -0 podman container exec --user git gitea gitea doctor check --all
  # Run all test but suppress output for gc-lfs
  ! echo -e "$output" | sed -e '/Garbage collect LFS/,+1d' | grep -q ERROR
}

@test "[ssh] Check OpenSSH is working over IPv4" {
  run -0 bash -c "echo '' | nc -4 localhost ${sshlisten_port}"
  echo "${output}" | grep -q 'SSH-2.0-OpenSSH'
}

@test "[ssh] Check OpenSSH is working over IPv6" {
  run -0 bash -c "echo '' | nc -6 localhost ${sshlisten_port}"
  echo "${output}" | grep -q 'SSH-2.0-OpenSSH'
}
