#!/usr/bin/env bash

ABSDIR="$( dirname "$(readlink -f -- "$0")" )"
source ${ABSDIR}/../functions.sh
source ${ABSDIR}/vars.sh

ensure_pwd_is_scriptdir
ensure_not_root

if podman volume exists ${srvdata_volume}; then
	echo "Files or volumes from previous installation already exists"
	echo "Please remove them before fresh install, or try continue with normal installation"
	exit 1
fi

podman volume create ${srvdata_volume}
${ABSDIR}/10_install.sh
