# PrivateBin

## Restauration
À partir d'une copie des home utilisateur (volumes podman compris) dans `/backup` :
* En tant qu'utilisateur `podman-privatebin` :
```
podman volume create privatebin-data
```
* En tant que `root` :
```
rsync -a /backup/home/podman-privatebin/.local/share/containers/storage/volumes/privatebin-data/_data ~podman-privatebin/.local/share/containers/storage/volumes/privatebin-data/
chown -R podman-privatebin:podman-users ~podman-privatebin/.local/share/containers/storage/volumes/privatebin-data/_data
```
* En tant qu'utilisateur `podman-privatebin` :
```
chown -R 65534:82 ~podman-privatebin/.local/share/containers/storage/volumes/privatebin-data/_data
```
* Exporter les variables d'environnement et procéder à l'installation normale (10_install.sh)
